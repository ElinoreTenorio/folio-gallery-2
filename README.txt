This is a fork of the original Folio Gallery script by FolioPages.com

http://www.foliopages.com/php-photo-gallery-no-database

This version has the following updates:

* Added upload functionality where you can upload a zip file and it will automatically unpack the zip and save it to the albums folder
* Added support to automatically resize PNG files as well
* Added random photos on the front page that shows random pics from any of your albums
* Facebook comment when viewing the album
* Implemented an HTML5 theme
* Sort folder by upload date
* Send email notification when a new photo album has been uploaded

To install:

1) Unzip the file to your web root
2) Grant "albums" folder a 777 permission and delete the "remove-this.txt" file in this folder.
3) Go to /upload and upload a zip of the image folder you want to upload
- when zipping images, put them in one folder and zip that folder. Upload the zipped file generated. The uploader asks for a PIN, you can set this by going to /upload/index.php on Line 3
4) Point to index.php file in your browser. Thumbnails will automatically be generated when you first visit the album folder in your site
- you can notify an email group that a new photo album has been uploaded. To know more, go to folio-gallery.php and review Line 10 to Line 13

Cheers!

Elinore Tenorio
e: elinore.tenorio@gmail.com
w: http://obitwaryo.com