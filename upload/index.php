<?php
// set any 4 digit number as PIN to access the image uploader
$pins = array(1234,2345);

if (!empty($_POST) && !isset($_POST['pin']) && !in_array($_POST['pin'], $pins)) {
	die("You are not authorized to upload an album");
	exit;
	
} elseif (isset($_POST['pin']) && in_array($_POST['pin'], $pins))  {

	$destination = '../albums/';
	include 'pclzip.lib.php';

	if(isset($_FILES["zip_file"]) && $_FILES["zip_file"]["name"]) {
		$filename = $_FILES["zip_file"]["name"];
		$source = $_FILES["zip_file"]["tmp_name"];
		$type = $_FILES["zip_file"]["type"];
	 
		$name = explode(".", $filename);
		$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
		foreach($accepted_types as $mime_type) {
			if($mime_type == $type) {
				$okay = true;
				break;
			} 
		}
	 
		$continue = strtolower($name[1]) == 'zip' ? true : false;
		if(!$continue) {
			$message = "The file you are trying to upload is not a .zip file. Please try again.";
		}
	 
		$target_path = $destination . $filename;  // change this to the correct site path
		if(move_uploaded_file($source, $target_path)) {
			$zip = new PclZip($target_path);
			if ($zip->extract(PCLZIP_OPT_PATH, $destination)) {
				unlink($target_path);
			}
			$message = "<h2>Success: Your .zip file was uploaded and unpacked!</h2>";
		} else {	
			$message = "There was a problem with the upload. Please try again.";
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<title>Upload Photos</title>

<link rel="stylesheet" type="text/css" href="../folio-gallery.css" />
<link rel="stylesheet" type="text/css" href="../fancybox/fancybox.css" />
</head>
<body>
<div align="center" style="font-size:13px;font-weight:bold;">
  <h2>Upload Images</h2>
</div>

<p>&nbsp;</p>

<div class="gallery">  
<?php if(isset($message) && $message) echo "<p>$message</p>"; ?>
<form enctype="multipart/form-data" method="post" action="">
<label>Choose a zip file to upload: <br /><input type="file" name="zip_file" /></label><br />
<label>PIN (4 digits): <br /><input type="password" name="pin" /></label><br /><br />
<input type="submit" name="submit" value="Upload" />
</form>

<div align="center">
	<a href="../index.php">Back to Albums</a>
</div>
</div>   

</body>
</html>