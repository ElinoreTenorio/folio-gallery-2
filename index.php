<?php
error_reporting (E_ALL ^ E_NOTICE);

$baseURL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$title = "Folio Gallery fork by RedJumpsuit";

if (isset($_GET['album'])) { $_GET['album'] = filter($_GET['album']); }
if (isset($_GET['p'])) { $_GET['p'] = filter($_GET['p']); } 

// filter data
function filter($data) {
    $data = trim(htmlentities(strip_tags($data)));
	if (get_magic_quotes_gpc())
        $data = stripslashes($data);
        $data = str_replace('/', '', $data);
        $data = str_replace('.', '', $data);
    return $data;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
  <?php 
    if (isset($_GET['album'])) {
	  echo $_GET['album'] .' | '. $title;
	} else {
	  echo $title;
	}
  ?>
</title>

<!-- start gallery header --> 
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>

<link rel="stylesheet" type="text/css" href="fancybox/fancybox.css" />
<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.1.min.js"></script>
<script type="text/javascript">
// fancy box settings
$(document).ready(function() {	
	$("a.albumpix").fancybox({
		'autoScale	'		: true, 
		'hideOnOverlayClick': true,
		'hideOnContentClick': true
	});
});
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</head>
<body>

<div class="main-indents">
    <div class="main">
        <!-- Header -->
        <header>
            <nav>
                <ul class="sf-menu">
					<li><a href="#"><?php echo $title; ?></a></li>
					<?php if (isset($_GET['album']) && $_GET['album'] != '') {?>
					<li><a href="index.php" class="current">all albums</a></li>
					<?php }?>
                </ul>
            </nav>
            <div class="clear"></div>
        </header>
		<section id="content"><div class="ic"></div>
			<div class="container_12">
		
				<?php include "folio-gallery.php"; ?>

			</div>

		</section>

        <!-- Footer -->
		<p>&nbsp;</p>
        <footer>
            <div class="copyright">
                © <?php echo $title; ?> 2012
            </div>
            <ul class="social-list">
            <li>&nbsp;</li>
            </ul>
        </footer>
    </div>
</div>

<!-- Start of StatCounter Code -->
<script type="text/javascript">
var sc_project=8350680; 
var sc_invisible=1; 
var sc_security="6efdbf56"; 
</script>
<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script>
<!-- End of StatCounter Code -->

</body>
</html>