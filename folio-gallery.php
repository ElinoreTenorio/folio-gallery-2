<?php 
// photo gallery settings
$mainFolder    = 'albums';   // folder where your albums are located - relative to root
$albumsPerPage = '12';       // number of albums per page
$itemsPerPage  = '18';       // number of images per page    
$thumb_width   = '120';      // width of thumbnails
$thumb_height  = '85';       // height of thumbnails
$extensions    = array(".jpg",".png",".gif",".JPG",".PNG",".GIF"); // allowed extensions in photo gallery

// if you are sending notification to an email group
$notify = true; 				// set to true or false if you want to send notification
$to = 'newuser@localhost.com';	// this can be a google group or yahoo groups email list
$from = 'admin@localhost.com';

// create thumbnails from images
function make_thumb($folder,$src,$dest,$thumb_width) {

	$ext = pathinfo($src, PATHINFO_EXTENSION);
	if ($ext == 'jpg' || $ext == 'JPG') {
		$source_image = imagecreatefromjpeg($folder.'/'.$src);
	} elseif ($ext == 'png' || $ext == 'PNG') {
		$source_image = imagecreatefrompng($folder.'/'.$src);
	}
	
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	$thumb_height = floor($height*($thumb_width/$width));
	
	$virtual_image = imagecreatetruecolor($thumb_width,$thumb_height);
	
	imagecopyresampled($virtual_image,$source_image,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
	
	if ($ext == 'jpg' || $ext == 'JPG') {
		imagejpeg($virtual_image,$dest,100);
	} elseif ($ext == 'png' || $ext == 'PNG') {
		imagepng($virtual_image,$dest,9);
	}
	
} 

// display pagination
function print_pagination($numPages,$urlVars,$currentPage) {
        
   if ($numPages > 1) {
      
	   echo 'Page '. $currentPage .' of '. $numPages;
	   echo '&nbsp;&nbsp;&nbsp;';
   
       if ($currentPage > 1) {
	       $prevPage = $currentPage - 1;
	       echo '<a href="?'. $urlVars .'p='. $prevPage.'">&laquo;&laquo;</a> ';
	   }	   
	   
	   for( $e=0; $e < $numPages; $e++ ) {
           $p = $e + 1;
       
	       if ($p == $currentPage) {	    
		       $class = 'current-paginate';
	       } else {
	           $class = 'paginate';
	       } 
	       

		       echo '<a class="'. $class .'" href="?'. $urlVars .'p='. $p .'">'. $p .'</a>';
		  	  
	   }
	   
	   if ($currentPage != $numPages) {
           $nextPage = $currentPage + 1;	
		   echo ' <a href="?'. $urlVars .'p='. $nextPage.'">&raquo;&raquo;</a>';
	   }	  	 
   
   }

}

// get random images
function get_images($root) {
	$r = array();
	foreach(glob("$root/*") as $n) {
		if (is_dir($n)) {
			$r = array_merge($r, get_images($n));
		} else {
		   $r[] = $n;
		}
	}
	return $r;
}


if (!isset($_GET['album'])) {

    $folders = array_diff(scandir($mainFolder), array('..', '.'));
	$sort_folders = array();
	
	foreach ($folders as $key=>$folder) {
		$stat_folders = stat($mainFolder .'/'. $folder);
		$folder_time[$key] = $stat_folders['ctime'];
	}
	
	if (count($folders) > 0) {
		array_multisort($folder_time, SORT_DESC, $folders);
	}

	$ignore  = array('.', '..', 'thumbs');
		  
	$albums = array();
	$captions = array();
	$random_pics = array();
	  
    foreach($folders as $album) {
         
	    if(!in_array($album, $ignore)) {    
			 
		   array_push( $albums, $album );
			 
		   $caption = substr($album,0,20);
		   array_push( $captions, $caption );
			 
		   $rand_dirs = glob($mainFolder.'/'.$album.'/thumbs/*.*', GLOB_NOSORT);
           $rand_pic  = $rand_dirs[array_rand($rand_dirs)];
		   array_push( $random_pics, $rand_pic );
		  
		 }
		  
	 }

  
     if( count($albums) == 0 ) {
  
        echo '<article class="a2">
				<h3>There are currently no albums.</h3>
			</article>';     
  
     } else {
  
		$numPages = ceil( count($albums) / $albumsPerPage );

        if(isset($_GET['p'])) {
      
	        $currentPage = $_GET['p'];
            if($currentPage > $numPages) {
               $currentPage = $numPages;
            }

         } else {
            $currentPage=1;
         } 
 
         $start = ( $currentPage * $albumsPerPage ) - $albumsPerPage;
	  
		echo '<article class="a2">
				<h3>Photo Albums ('.count($albums).')</h3>
				<div class="wrapper">';					
					for( $i=$start; $i<$start + $albumsPerPage; $i++ ) {
					if( isset($albums[$i]) ) {
					
						echo '<div class="col-5 thumb">
								<figure class="img-box">
									<a href="'.$_SERVER['PHP_SELF'].'?album='. urlencode($albums[$i]) .'" class="lightbox-image">
										<img src="'. $random_pics[$i] .'" width="'.$thumb_width.'" height="'.$thumb_height.'" alt="" />
									</a>
								</figure>
								<div class="gallery-meta">
									<a href="'.$_SERVER['PHP_SELF'].'?album='. urlencode($albums[$i]) .'" class="gallery-name">'. $captions[$i] .'</a>
								</div>
							</div>';
						}		  	  
					}
					
			echo '</div>
				</article>';
  
          echo '<div align="center" class="paginate-wrapper">';
        	 
                 $urlVars = "";
                 print_pagination($numPages,$urlVars,$currentPage);
  
          echo '</div>';

		// display random images
		$ignore_once  = array('.', '..');
		$files = array();
		foreach($folders as $folder) {
			if (!in_array($folder, $ignore_once)) {
				$files = array_merge($files, get_images($mainFolder . '/'. $folder . '/thumbs'));
				shuffle($files);				
			}
		}		
		
		echo '<article class="content-box">
                	<h3>Random Photos</h3>
                    <div class="wrapper p3">';
					
					for($im=0; $im<$itemsPerPage; $im++) {
						if (isset($files[$im])) {
							$rand_file = explode('/', $files[$im]);
						
							echo '<div class="col-5 thumb">
									<figure class="img-box">
										<a href="'. $mainFolder .'/'. $rand_file[1] .'/'. $rand_file[3] .'" class="albumpix" rel="albumpix">
											<img src="'. $files[$im] .'" width="'.$thumb_width.'" height="'.$thumb_height.'" alt="" />
										</a>
									</figure>
								</div>';
						}
						
					}
		echo '  </div>
                </article>';
     }
   

} else {

     // display photos in album
     $src_folder = $mainFolder.'/'.$_GET['album'];
     $has_sent = false;
     if (is_dir($src_folder)) {
		$src_files  = scandir($src_folder);
		$my_folder = true;
	 } else {
		$my_folder = false;
	 }

     $files = array();
     if ($src_files) {
		 foreach($src_files as $file) {
			
			$ext = strrchr($file, '.');
			if(in_array($ext, $extensions)) {
			  
			   array_push( $files, $file );
			  
			   
			   if (!is_dir($src_folder.'/thumbs')) {
				  mkdir($src_folder.'/thumbs');
				  chmod($src_folder.'/thumbs', 0777);
			   }
			   
			   $thumb = $src_folder.'/thumbs/'.$file;
			   
			   if (!file_exists($thumb)) {
				  make_thumb($src_folder,$file,$thumb,$thumb_width); 
				  $has_sent = true;
		
			   }
			
			 }
		  
		  }
		  
		// mail the group
		if ($has_sent && $notify) {
			$subject = 'New photo album uploaded: '. $_GET['album'];
			$message = $baseURL;
			$headers = "From: {$from} \r\n" .
				"Reply-To: {$from} \r\n" .
				"X-Mailer: PHP/". phpversion();
	
			mail($to, $subject, $message, $headers);
		}
		
	 }
 

   if ( count($files) == 0 ) {

     if ($my_folder) {
		echo '<article class="a2">
				<h3>There are no photos in this album!</h3>
            </article>';
		} else {
		echo '<article class="a2">
				<h3>This album does not exist.</h3>
            </article>';
		}
   
   } else {
   
      $numPages = ceil( count($files) / $itemsPerPage );

      if(isset($_GET['p'])) {
      
	     $currentPage = $_GET['p'];
         if($currentPage > $numPages) {
            $currentPage = $numPages;
         }

      } else {
         $currentPage=1;
      } 

   $start = ( $currentPage * $itemsPerPage ) - $itemsPerPage;

   echo '<article class="a2">
				<h3>'. $_GET['album'] .' ('.count($files).')</h3>
				<div class="wrapper">';	

				for( $i=$start; $i<$start + $itemsPerPage; $i++ ) {
					  if( isset($files[$i]) && is_file( $src_folder .'/'. $files[$i] ) ) { 
						
						echo '<div class="col-5 thumb">
									<figure class="img-box">
										<a href="'. $src_folder .'/'. $files[$i] .'" class="albumpix" rel="albumpix">
											<img src="'. $src_folder .'/thumbs/'. $files[$i] .'" width="'.$thumb_width.'" height="'.$thumb_height.'" alt="" />
										</a>
									</figure>
								</div>';
						} elseif(isset($files[$i])) {
						  echo $files[$i];
						}
					   
					  }
					   
		echo '</div>
		</article>';				
	}  
  
     echo '<div align="center" class="paginate-wrapper">';
        	 
        $urlVars = "album=".urlencode($_GET['album'])."&amp;";
        print_pagination($numPages,$urlVars,$currentPage);
  
     echo '</div>';
     
echo '<article class="a2">';
if (isset($_GET['album'])) {
 echo '<div class="fb-comments" data-href="'. $baseURL .'/index.php?album="'. $_GET['album'] .'" data-num-posts="5" data-width="500"></div>';
}
echo '</article>';
	 
	 
   } // end else	 


?>